"use strict";
const runApp = () => {
    let counter = {
        last: 0,
        next() {
            return this.last++;
        }
    }

    const extractValue = (id) => {
        return $(id).val();
    }

    const makeTaskObject = () => {
        const task = {
            name: extractValue("#field-task"),
            id: counter.next(),
        };
        return task;
    }

    const removeFromTODO = (array, i) => {
        array.splice(i, 1);
        todoArray.splice(i, 1);
        todoIter--;
        const asJson = JSON.stringify(array);
        localStorage.setItem("todo", asJson);
    }

    const removeFromInProgress = (array, i) => {
        array.splice(i, 1);
        inProgressArray.splice(i, 1);
        inProgressIter--;
        const asJson = JSON.stringify(array);
        localStorage.setItem("inProgress", asJson);
    }

    const removeFromDone = (array, i) => {
        array.splice(i, 1);
        doneArray.splice(i, 1);
        doneIter--;
        const asJson = JSON.stringify(array);
        localStorage.setItem("done", asJson);
    }

    const deleteElement = (element, id) => {
        const deleteTask = element.find(".remove");
        deleteTask.on("click", function (event) {
            if ($(event.target).parents("#todo").attr("id") === "todo") {
                const array = JSON.parse(localStorage.getItem("todo"));
                for (let i = 0; i < array.length; i++) {
                    const object = JSON.parse(array[i]);

                    if (object.id === id) {
                        removeFromTODO(array, i);
                    }
                }
            }
            else if ($(event.target).parents("#in-progress").attr("id") === "in-progress") {
                const array = JSON.parse(localStorage.getItem("inProgress"));
                for (let i = 0; i < array.length; i++) {
                    const object = JSON.parse(array[i]);

                    if (object.id === id) {
                        removeFromInProgress(array, i);
                    }
                }
            }
            else {
                const array = JSON.parse(localStorage.getItem("done"));
                for (let i = 0; i < array.length; i++) {
                    const object = JSON.parse(array[i]);

                    if (object.id === id) {
                        removeFromDone(array, i);
                    }
                }
            }
            element.remove();
        });
    }
    let todoIter = 0;
    let inProgressIter = 0;
    let doneIter = 0;
    const todoArray = [];
    const inProgressArray = [];
    const doneArray = [];

    const saveTaskTODO = (task) => {
        todoArray[todoIter] = JSON.stringify(task);
        const asJson = JSON.stringify(todoArray);
        localStorage.setItem("todo", asJson);
        todoIter++;
    }

    const saveTaskInProgress = (task) => {
        inProgressArray[inProgressIter] = JSON.stringify(task);
        const asJson = JSON.stringify(inProgressArray);
        localStorage.setItem("inProgress", asJson);
        inProgressIter++;
    }

    const saveStateDone = (task) => {
        doneArray[doneIter] = JSON.stringify(task);
        const asJson = JSON.stringify(doneArray);
        localStorage.setItem("done", asJson);
        doneIter++;
    }


    const makeElement = (task) => {
        const nameTask = task.name;
        const idTask = task.id;

        //шаблон для задачи
        //поставить в шаблон значения из объекта задачи
        const contents =
            `<div class="flex flex-content margin-bottom">
            <span class="content">${nameTask}</span>
            <button class="btn remove padding">X</button>
            </div>`;
        const buttonInProgress = `<button class="btn move-to-in-progress">In Progress ></button>`;
        const buttonDone = `<button class="btn move-to-done">Done ></button>`;
        const buttonDelete = `<button class="btn remove-task">Delete</button>`;

        //создать сам элемент и добавить в него содержимое
        const element = $("<div>");
        element.addClass("task flex flex-vert margin-bottom");
        element.html(contents + buttonInProgress);

        //добавить событие на кнопку (на удаление)
        deleteElement(element, idTask);

        let move = element.find(".move-to-in-progress");
        move.on("click", function () {
            const answer = confirm("Are you sure you want to move the task?");
            if (answer) {
                const array = JSON.parse(localStorage.getItem("todo"));
                for (let i = 0; i < array.length; i++) {
                    const object = JSON.parse(array[i]);

                    if (object.id === idTask) {
                        saveTaskInProgress(object);
                        removeFromTODO(array, i);
                    }
                }

                element.find(".move-to-in-progress").remove();
                element.append($(buttonDone));

                deleteElement(element, idTask);

                move = element.find(".move-to-done");
                move.on("click", function () {
                    const answer = confirm("Are you sure you want to move the task?");
                    if (answer) {
                        const array = JSON.parse(localStorage.getItem("inProgress"));
                        for (let i = 0; i < array.length; i++) {
                            const object = JSON.parse(array[i]);

                            if (object.id === idTask) {
                                saveStateDone(object);
                                removeFromInProgress(array, i);
                            }
                        }

                        element.find(".move-to-done").remove();
                        element.append($(buttonDelete));

                        deleteElement(element, idTask);

                        const remove = element.find(".remove-task");
                        remove.on("click", function (e) {
                            $(e.target).parents(".task").remove();

                            const array = JSON.parse(localStorage.getItem("done"));
                            for (let i = 0; i < array.length; i++) {
                                const object = JSON.parse(array[i]);

                                if (object.id === idTask) {
                                    removeFromDone(array, i);
                                }
                            }
                        });

                        $("#done .tasks").append(element);
                    }
                });

                $("#in-progress .tasks").append(element);
            }
        });

        //добавить элемент в контайнер задач
        $("#todo .tasks").append(element);
    }

    const addTask = () => {
        //Получить данные из инпута
        const task = makeTaskObject();

        //Создать элемент с данными задачи
        makeElement(task);

        //Сохранение 
        saveTaskTODO(task);
    }

    window.addEventListener("DOMContentLoaded", () => {
        $("#button-add").on("click", addTask);
    });
}
runApp();